<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', '_PASSWORD_' );

/** MySQL hostname */
define( 'DB_HOST', '_DBHOST_' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!/8c$Q##Pl=W~+e7-ey`{+jS~b]rm%ikD%}*Q|@VHI|x0b^Q$Qn<tj+)iFjZj:Xb');
define('SECURE_AUTH_KEY',  '5-Ku~5h3qGu5+9-;6Onm%rA6mn5W%.lo?z7|]P^_jP5v3,[C^g0iehV{boi342K#');
define('LOGGED_IN_KEY',    'WXz5eiT,qYR-+lb-CExPsn$zo_tU~!R;aIxzge^/Mn[)h62<[<0O$0$^--=rmT>H');
define('NONCE_KEY',        '2|Qd+_TI1$2i6_=96owoVIu[Cdj:enR=+Kt/y~Lb1EeU/)d&v)YZxstY0L:93,Sz');
define('AUTH_SALT',        '( *(nf$L?9(l7YHE^]S]mFG1i_XA$5TaQI|5cwV^rh0/WoSF7BO/uSm}vEA&o0Z!');
define('SECURE_AUTH_SALT', 'TJ+PT|CZb8}z}X(lDOELpiFMSmtrzhdAKQ*R/hHBt1MnX?:+vI><1*VwtkXSF7,`');
define('LOGGED_IN_SALT',   'NH;4QNEB2%@_C/%#EybS|vgpzlGRg9Q<wZ|A:?7ogVc5a)WCmii.6j/K{%n|%dIQ');
define('NONCE_SALT',       '@VcZ.#zx[wS[p8VSSqE9DtVPE{$Gh/ovN7cz,I+-K$-zo`4UR:j1MDIiD`1(pucT');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
