#!/usr/bin/env bash

# TODO: à convertir en nftables

TRUSTCLIENT=192.168.105.79 # ma station à moi

TRUSTNET=192.168.105.0/24

SERVICES="http https"

iptables -F INPUT
iptables -P INPUT DROP

iptables -A INPUT -p tcp --dport 22 -s $TRUSTCLIENT -j ACCEPT

for s in $SERVICES
do
  iptables -A INPUT -p tcp --dport $s -j ACCEPT
done

iptables -A INPUT -p icmp -j ACCEPT

# à ne pas oublier sur un routeur
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

