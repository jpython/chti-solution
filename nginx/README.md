# Déployer Wordpress, MariaDB et un frontal NGINX avec Docker Compose

Nous pouvons utiliser une VM Debian, CentOS ou Ubuntu.

En suivant cette documentation :

https://upcloud.com/community/tutorials/deploy-wordpress-with-docker-compose/

![](diagram1.png)

Quelques adaptations :
- Installer docker-compose avec apt : `apt install docker-compose`
- Arrêter le firewall sur l'hôte :
  - Ubuntu : `sudo systemctl stop ufw && sudo systemctl disable ufw`
  - CentOS : `sudo systemctl stop firewalld && sudo systemctl disable firewalld`
- Pour débuguer un site Web : extension Web developper pour Firefox :
https://addons.mozilla.org/fr/firefox/addon/web-developer/
- Récupérer le docker-compose.yml dans le répertoire wordpress-compose
- Adapter le mot de passe et l'adresse ip
- Ajouter dans `/etc/hosts` : `public_ip   me.mydomain.org`
- Lancer `sudo docker-compose up -d`
- Aller sur http://me.mydomain.org pour initialiser Wordpress (modifier `/etc/hosts`
sur l'hôte)

Nous allons faire tourner Wordpress et MariaDB dans deux conteneurs Docker
et les déployer avec Docker Compose.

~~~~yaml
wordpress:
    image: wordpress
    links:
     - mariadb:mysql
    environment:
     - WORDPRESS_DB_PASSWORD=c1estN0trePROJET!
    ports:
       - "80"
    volumes:
     - ./html:/var/www/html
mariadb:
    image: mariadb
    environment:
     - MYSQL_ROOT_PASSWORD=c1estN0trePROJET!
     - MYSQL_DATABASE=wordpress
    volumes:
     - ./database:/var/lib/mysql
~~~~

Nous allons examiner le contenu des conteneurs déployés pour contrôler
ce que l'on fait.

Pour chaque conteneur :
- Trouver son id avec `docker ps -a`
- S'y connecter : `sudo docker exec -it id_du_conteneur bash`
- Chercher `/etc/debian*, /etc/redhat*`
- Utiliser `dpkg` ou `rpm` pour voir les paquets installés et leur version.
- La base Linux : quelle distribution, quelle version
- Le serveur Web utilisé
- Les versions des composants : Wordpress, serveur Web, MariaDB, etc.

Stockage : Dans un premier temps nous stockons les volumes docker dans
le répertoire courant : `./database` et `./html`, ensuite nous trouverons
une meilleure solution.

## Conteneur Wordpress :

- Base de l'OS : Debian 10
- Serveur Web : Apache 2

## Conteneur MariaDB :
- Base de l'OS :
- Version de mariadb-server :

Ensuite nous ajouterons un frontal NGINX (mandataire inverse) et y installer
un certificat SSL auto-signé.

## Une image docker contenant NGINX orientée proxy avec une gestion des certificats SSL

Les machines sont dans un réseau internet ce qui évite d'avoir à changer
les numéro de port. Seule le conteneur nginx est accessible de l'extérieur.

La ligne link: fait en sorte que le DNS interne de Docker associe le 
nom wordpress à l'IP interne du conteur wordpress.

~~~~yaml
version: '3'
services:
  reverse:
      image: nginx
      links:
        - wordpress:wordpress
      container_name: nginx
      ports:
       - 80:80
       - 443:443
      volumes:
       - ./nginx/etc/nginx:/etc/nginx
       - ./nginx/etc/ssl:/etc/ssl
      networks:
       - cluster-internal
  wordpress:
      image: wordpress
      links:
       - mariadb:mysql
      environment:
       - WORDPRESS_DB_PASSWORD=c1estN0trePROJET!
      expose:
          - "80"
      volumes:
       - ./html:/var/www/html
      networks:
       - cluster-internal
  mariadb:
      image: mariadb
      environment:
       - MYSQL_ROOT_PASSWORD=c1estN0trePROJET!
       - MYSQL_DATABASE=wordpress
      volumes:
       - ./database:/var/lib/mysql
      networks:
       - cluster-internal

networks:
  cluster-internal:
~~~~

le fichier `nginx.conf` correspondant est :

~~~~
http {
  upstream realsrv {
          server wordpress:80 weight=1;
  }
  server {
     listen 80;
     # pour tester ajouter dans /etc/hosts
     # me.mydomain.org : ip publique de l'hôte
     server_name me.mydomain.org;
     location / {
       proxy_pass http://realsrv;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
     } 
  }
}

events {
        worker_connections 768;
        # multi_accept on;
}
~~~~

Les modifications de l'en-tête HTTP par le proxy font en sorte que tous
les liens dans les pages de Wordpress (css, etc.) et les redirections
pointent bien sur le nom public du site (me.mydomain.org) et que le
site Wordpress s'initialise avec ce nom dans sa base de donnée aussi.

## Modifier le fichier compose pour ajouter un second Wordpress

Un second Wordpress derrière le mandataire (_load balancing_))
NGINX utilisant la même base de données.

~~~~yaml
version: '3'
services:
  reverse:
      image: nginx
      links:
        - wordpress:wordpress
        - wordpress2:wordpress2
      container_name: nginx
      ports:
       - 80:80
       - 443:443
      volumes:
       - ./nginx/etc/nginx:/etc/nginx
       - ./nginx/etc/ssl:/etc/ssl
      networks:
       - cluster-internal
  wordpress:
      image: wordpress
      links:
       - mariadb:mysql
      environment:
       - WORDPRESS_DB_PASSWORD=c1estN0trePROJET!
      expose:
          - "80"
      volumes:
       - ./html:/var/www/html
      networks:
       - cluster-internal
  wordpress2:
      image: wordpress
      links:
       - mariadb:mysql
      environment:
       - WORDPRESS_DB_PASSWORD=c1estN0trePROJET!
      expose:
          - "80"
      volumes:
       - ./html:/var/www/html
      networks:
       - cluster-internal
  mariadb:
      image: mariadb
      environment:
       - MYSQL_ROOT_PASSWORD=c1estN0trePROJET!
       - MYSQL_DATABASE=wordpress
      volumes:
       - ./database:/var/lib/mysql
      networks:
       - cluster-internal

networks:
  cluster-internal:
~~~~

`nginx.conf` est modifié en conséquence :
~~~~
...
  upstream realsrv {
          server wordpress:80 weight=1;
          server wordpress2:80 weight=1;
  }
...
~~~~

`docker logs _id_d'un_wordpress` permet de vérifier que les requètes sont partagées
entre les deux serveurs et que les logs reflètent bien l'IP d'origine du clients.

## Installation des certificats et configuration de SSL



~~~~
$ cd nginx/etc/ssl

$ openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout private/nginx-selfsigned.key \
    -out ./nginx-selfsigned.crt

...
Generating a RSA private key
......................+++++
..........................+++++
writing new private key to 'private/nginx-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:Haut-de-France
Locality Name (eg, city) []:Lille
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Flying Circus
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:me.mydomain.org
Email Address []:contact@mydomain.org

$ openssl dhparam -out dhparam.pem 2048
~~~~

On crée deux fichiers annexes de configuration de NGINX :

`etc/nginx/self-signed.conf` :

~~~~
ssl_certificate /etc/ssl/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
~~~~

`etc/nginx/ssl-params.conf` : 

~~~~
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/ssl/dhparam.pem;
ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
~~~~

On remplace la section `server` de nginx.conf par deux sections (une pour https,
l'autre pour http qui redirige le client vers le site chiffré.

~~~~
  server {
    listen 443 ssl;
    listen [::]:443 ssl;
    include ./self-signed.conf;
    include ./ssl-params.conf;

    server_name me.mydomain.org;

    location / {
       proxy_pass http://realsrv;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
     } 
  }

  server {
    listen 80;
    listen [::]:80;

    server_name me.mydomain.org;

    return 302 https://$server_name$request_uri;
}
~~~~

Vérifiez qu'une connexion sur http://me.mydomain.org redirige bien en https.
Le navigateur râle parce que le certificat est auto-signé, il faudra penser
à utiliser LetsEncrypt pour un site accessible au public.

## Mise au point

En cas de problème de lancement du pseudo-cluster :
~~~~
$ docker-compose up -d
$ docker ps -a
~~~~

Regarder les logs du conteneur qui a des soucis de lancement ou de
fonctionnement : `docker logs id_du_conteneur` 

Vérifier les chemins d'accès aux fichiers dans la configuration et
leurs emplacements réels :

~~~~
$ tree nginx
nginx
└── etc
    ├── nginx
    │   ├── nginx.conf
    │   ├── self-signed.conf
    │   └── ssl-params.conf
    └── ssl
        ├── dhparam.pem
        ├── nginx-selfsigned.crt
        └── private
            └── nginx-selfsigned.key
~~~~ 

Vérifier qu'il n'y a pas deux conteneurs qui ouvrent le même port sur
la même interface ou s'il n'y a pas le même service sur la machine
hôte : `lsof -i` ou `ss`



# Que cherche-t-on à faire ensuite ?

Déployer d'une façon assez proche les mêmes conteneurs sur plusieurs machines
virtuelles sur une plate-forme de cloud.

![](K8S-A.png)

1. Il faut créer les machines virtuelles destinées à héberger K8s
   de façon automatique.
   - Le module boto3 de Python pour déployer dans AWS ou plate-forme compatible
   - Terraform pour déployer dans AWS, Microsoft Azure, Google Compute
   - à ce stade les VMs sont créés et accessibles en ssh avec votre keypair

2. Déployer K8s sur les VMs créées : noeud d'admin et workers
   - à la main, c'est bien, mais c'est long, on peut s'appuyer sur
     https://github.com/kelseyhightower/kubernetes-the-hard-way
   - on peut l'automatiser avec des outils puppet, salt, ansible
     (on peut trouver des "playbook" ansible pour K8s) ou comme
     dans le fil rouge 1 avec Python

3. Ensuite il faut y déployer (un peu comme avec doker compose ici)
   ngix/wordpress * n et mariadb, il y un fichier yaml simlaire
   à docker-compose.yml pour de demander.
   ~~~~
    $ kubadm deploy moncluster.yml
   ~~~~

4. Gérer l'élasticité du cluster si forte charge (ou sur demande de l'admin)
   1. On crée une ou plusieurs VMs
   2. On les configure commer worker et on les enregistre dans K8s
   3. Les conteneurs vont aller se répartir dessus

# Détails à ne pas ignorer

- Stockage permanent : /var/lib/mysql du conteneur MariaDB, les logs
  du serveur NGINX (/var/log/...)
  1. La plate-forme de cloud peut proposer ce service (NFS, ...)
  2. On peut monter une VM rien que pour ça avec ou sans conteneur,
     par exemple un serveur NFS sous Linux
  3. Kubernetes permets de gérer les demandes de stockage permanent
     des conteurs (_volume claims_)

- La déclaration des services :
  - Quand un conteneur Wordpress nouveau est lancé il faut qu'il
    soit pris en compte par NGINX
  - Une solution : des enregistrements dans le DNS du cluster K8s
    sont créés :
    
~~~~
       _http._tcp.internal IN SRV 80 1 0 wordpress44.internal.
       wordpress44         IN A 10.1.42.231
~~~~

   - Dans le bloc upstream de la configuration de NGINX on peut
     demander à prendre en compte le DNS pour connaître les
     serveurs Wordpress.

## À évaluer

L'outil `kompose` convertit un docker-compose.yml en fichiers
de déploiement Kubernetes :

~~~~
$ mkdir /tmp/testk
$ cp docker-compose.yml /tmp/testk
$ cd /tmp/testk
$ curl -L https://github.com/kubernetes/kompose/releases/download/v1.19.0/kompose-linux-amd64 -o kompose
$ chmod +x kompose
$ ./kompose
...
WARN Volume mount on the host "/tmp/b/testk/html" isn't supported - ignoring path on the host 
INFO Kubernetes file "reverse-service.yaml" created 
INFO Kubernetes file "mariadb-deployment.yaml" created 
...
$ ls
docker-compose.yml                         reverse-service.yaml
mariadb-claim0-persistentvolumeclaim.yaml  wordpress2-claim0-persistentvolumeclaim.yaml
mariadb-deployment.yaml                    wordpress2-deployment.yaml
reverse-claim0-persistentvolumeclaim.yaml  wordpress-claim0-persistentvolumeclaim.yaml
reverse-claim1-persistentvolumeclaim.yaml  wordpress-deployment.yaml
reverse-deployment.yaml
~~~~

La partie stockage n'est pas prise en compte (volumes) il faut repasser
sur les fichiers. K8s fournit des services de stockage (NFS par example)
(voir les fichiers *volumeclaim*).

# Ça vous paraît jouable pour début janvier ?

