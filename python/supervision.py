#!/usr/bin/env python3
'''Supervision d'une machine distante'''

import sys
from runcheck import runremote
import sqlite3

database = '/home/ib/tp1/chti-solution/sql/supervision.db'

try:
    me,host = sys.argv
    if not host:
        raise ValueError
except ValueError:
    usage = '{} machine\n'.format(sys.argv[0])
    sys.stderr.write(usage)
    exit(1)

sout, serr, stat = runremote(host, 'cat /proc/loadavg')
l1,l5,l15,*_ = sout.split()

hostname,sout,stat = runremote(host,'hostname')

nhttpd,serr,stat = runremote(host,'pgrep -c apache2') # httpd sur CentOS
nhttpd = int(nhttpd)

dfout,serr,stat = runremote(host,'df -h / | tail -n 1') 
avail = dfout.split()[4]
avail = float(avail.rstrip('%'))

with sqlite3.connect(database) as c:
    curs = c.cursor()
    curs.execute("INSERT INTO supervision(host,l1,l5,l15,nhttpd,avail) " \
                 " VALUES(?,?,?,?,?,?)",
                 (hostname,l1,l5,l15,nhttpd,avail) )


