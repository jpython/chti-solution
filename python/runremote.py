#!/usr/bin/env python3

import paramiko
import sys
from handle_ssh import connect_to_host

try:
    me, command, *hosts = sys.argv
    if not hosts:
        raise ValueError
except ValueError:
    usage = '{} commande ip ...\n'.format(sys.argv[0])
    sys.stderr.write(usage)
    exit(1)

c = paramiko.SSHClient()

#private_keyfile = os.path.expanduser('~/.ssh/id_rsa')
#if os.path.isfile(private_keyfile): 
#    k = paramiko.RSAKey.from_private_key_file(private_keyfile)
#else:
#    sys.stderr.write("Impossible d'ouvrir {}.\n".format(private_keyfile))
#    exit(2)

c.load_system_host_keys()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

paramiko.util.log_to_file("paramiko.log", level = "INFO")

display = '{:15}\t{}'

for host in hosts:
    connect_to_host(c,host)
    stdin, stdout, stderr = c.exec_command(command)
    status = stdout.channel.recv_exit_status()
    print(display.format(
            host, 
            stdout.read().decode('UTF-8').rstrip()
            ))
    print('Status :', status)

