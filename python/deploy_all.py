#!/usr/bin/env python3
'''Installe maradb et déploie Wordpress'''

import sys
import string
import io
import pymysql
import paramiko
from runcheck import runremote
from random import choice
from handle_ssh import connect_to_host
from scp import SCPClient

try:
    me,frontal,db = sys.argv
    if not frontal or not db:
        raise ValueError
except ValueError:
    usage = '{} frontal db\n'.format(sys.argv[0])
    sys.stderr.write(usage)
    exit(1)

def install_mariadb(host):
    print('Synchronisation dépôts.',end='',flush=True)
    out,err,status = runremote(host,'apt -y update')
    if status:
        print('. Échec.')
        exit(1)
    print('.. Fait.')

    print('Installation MariaDB.',end='',flush=True)
    out,err,status = runremote(host,'apt -y install mariadb-server')
    if status:
        print(' Échec.')
        print(err)
        exit(1)
    print('.. Fait.')


def get_db_passwords():
    try:
        with open('mariadb_password.txt') as input:
            for line in input: # lit jusqu'à la dernière ligne
                pass
            _ , _, dbroot_password, _, dbwp_password = line.strip().split()
    except (FileNotFoundError,IsADirectoryError,PermissionError):
        print('Impossible de lire le fichier de mot de passe !',file=sys.stderr)
        print('ABANDON')
        exit(1)
    return dbroot_password, dbwp_password

# requète choppée dans /usr/bin/mysql_secure_installation

def configure_mariadb(host):
    alphabet = string.ascii_letters + string.digits
    dbroot_password = ''.join(choice(alphabet) for i in range(10))
    dbwp_password   = ''.join(choice(alphabet) for i in range(10))

    update_dbroot = "UPDATE mysql.user SET Password=PASSWORD('{}') " \
                    "WHERE User='root';".format(dbroot_password)
    
    out,err,status = runremote(host,'echo "{}" | mariadb -u root'.format(update_dbroot))
    if status:
        print(' Échec.')
        print('On continue, ça a peut être été déjà fait.')
        print('récupération du dernier mot de passe dans mariadb_password.txt')
        dbroot_password, dbwp_password = get_db_passwords()
    else:
        with open('mariadb_password.txt','a') as out:
            out.write("\n{}: root: {} wp: {}".format(host,dbroot_password, dbwp_password))
    
    print("Écoute sur le port TCP/3306 par MariaDB.",end='',flush=True)
    out,err,status = runremote(host,
                               "sed -e 's/^#port/port/' " \
                               "-e 's/^\(bind-address *= *127.0.0.1\)/#\1/' " \
                               "--in-place=.bak "         \
                               "/etc/mysql/mariadb.conf.d/50-server.cnf")
    if status:
        print(' Échec.')
        print('On continue, ça a peut être été déjà fait.')
    else:
        print("\nRelance de mariadb.",end='',flush=True)
        out,err,status = runremote(host,"systemctl restart mariadb")
        if status:
            print(". Échec.")
            print("ABANDON")
            exit(1)
    
    print('\nSuppression/Création base wp et utilisateur wp.',end='',flush=True)
    create_db = "DROP DATABASE IF EXISTS wp; CREATE DATABASE wp;"
    grant_wp  = "GRANT ALL ON wp.* TO wp@'%' IDENTIFIED BY '{}';".format(dbwp_password)
    
    out,err,status = runremote(host,'echo "{}" | mariadb -u root'.format(create_db))
    out,err,status1 = runremote(host,'echo "{}" | mariadb -u root'.format(grant_wp))
    
    if status or status1:
        print('. Échec.')
        print('On continue, ça a peut être été déjà fait.')
    
    print('\nMot de passe MariaDB/wp: ', dbwp_password)
    with pymysql.connect(host, 'wp', dbwp_password, 'wp') as c:
        #cur = c.cursor()
        c.execute("SELECT VERSION()")
        version = c.fetchone()
        print("Database version: {}".format(version[0]))

def deploy_wordpress(host):
    listpackages = 'php7.3-common php7.3-mbstring php7.3-xmlrpc ' \
                   'php7.3-soap php7.3-gd php7.3-xml php7.3-intl ' \
                   'php7.3-mysql php7.3-cli php7.3-ldap php7.3-zip ' \
                   'php7.3-curl dos2unix'
    out,err,status = runremote(host,'apt -y install ' + listpackages)
    out,err,status = runremote(host,'systemctl restart apache2')
    out,err,status = runremote(host,'rm -rf /var/www/html')
    out,err,status = runremote(host,'tar xf /tmp/wp-latest.tar.gz -C /var/www')
    out,err,status = runremote(host,'mv /var/www/wordpress /var/www/html')
    out,err,status = runremote(host,'chown -R www-data:www-data /var/www/html')
    out,err,status = runremote(host,'chmod -R a=rwX,go=rX /var/www/html')
    out,err,status = runremote(host,r'find /var/www/html -type f -exec dos2unix {} \;')

def configure_wordpress(host,dbhost):
    conffile = '../conf/wp-config-template.php'
    _, dbwp_password = get_db_passwords()
    with open(conffile) as conf:
        content = conf.read()
        content = content.replace('_DBHOST_',dbhost)
        content = content.replace('_PASSWORD_',dbwp_password)

    fl = io.BytesIO()
    fl.write(content.encode('UTF-8'))
    fl.seek(0)

    c = paramiko.SSHClient()
    c.load_system_host_keys()
    c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    connect_to_host(c,host)
    with SCPClient(c.get_transport()) as scp:
        scp.putfo(fl, '/var/www/html/wp-config.php')

    runremote(host,'chown www-data:www-data /var/www/html/wp-config.php')
    runremote(host,'chmod a=rwX,go=rX /var/www/html/wp-config.php')


if __name__ == '__main__':
    install_mariadb(db)
    configure_mariadb(db)
    deploy_wordpress(frontal)
    configure_wordpress(frontal,'192.168.105.30')
