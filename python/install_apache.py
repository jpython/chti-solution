#!/usr/bin/env python3
'''Installe Apache et télécharge WP'''

import sys
from runcheck import runremote

try:
    me,host = sys.argv
    if not host:
        raise ValueError
except ValueError:
    usage = '{} machine\n'.format(sys.argv[0])
    sys.stderr.write(usage)
    exit(1)


print('Synchronisation dépôts.',end='',flush=True)
out,err,status = runremote(host,'apt -y update')
if status:
    print('. Échec.')
    exit(1)
print('.. Fait.')

print('Installation Apache/PHP.',end='',flush=True)
out,err,status = runremote(host,'apt -y install apache2 libapache2-mod-php')
if status:
    print(' Échec.')
    print(err)
    exit(1)
print('.. Fait.')


print('Téléchargement de Wordpress.',end='',flush=True)
out,err,status = runremote(host,'http_proxy=http://192.168.105.50:3128 ' \
                                'https_proxy=http://192.168.105.50:3128 ' \
         'wget -q https://wordpress.org/latest.tar.gz -O /tmp/wp-latest.tar.gz')
if status:
    print('. Échec.')
    print(err)
    exit(1)
print('.. Fait.')


