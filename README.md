# chti

**Projet fil rouge - première partie**

*Administration à distance d'un serveur Debian ou Centos en Python, déploiement de Wordpress sur des VMs*

L'objectif de ce projet et de réaliser et documenter le déploiement de deux machines virtuelles Virtual Box
hébergeant respectivement Apache2 et Wordpress sur l'une et une base MariaDB sur l'autre. La mise en œuvre
en sera automatisée au maximum pour pouvoir réaliser un déploiement similaire rapidement, y compris sur
une plate forme de Cloud IaaS (AWS, Azure, Google ou autre).

**Mise en jambe, un échauffement**

Vous devriez déjà avoir créé une machine virtuelle sous Debian et une autre sous CentOS.
1.  Vérifiez que vous pouvez vous y connecter à partir de votre compte utilisateur sur la station Ubuntu avec ssh en tant que root et sans spécifier de mot de passe.

      `ssh root@ip_machine whoami` doit répondre `root`. 
      
      connecté via la console de Virtual Box la commande `ip addr show` permet de connaître l'IP de la machine.
      Si ça ne fonctionne pas **FAITES APPEL À VOTRE FORMATEUR !**
      
2.  Écrivez un script à utiliser ainsi  `update_linux.py addresse_ip` qui à partir de la station Ubuntu déclenche une mise à jour des paquets (logiciels) installés en 
vérifiant si la machine est une Debian-like (Debian, Ubuntu, Mint, etc.) ou Redhat-like (RHEL, CentOS, Fedora) et exécute à distance les commandes
appropriées pour mettre à jour les logiciels installés (paquetages). Faites en sorte que le script vérifie que tout s'est bien passé ou non et le signale.

**Indices :**
* Si vous préfixez une commande par `LANG=C` vous l'obligez à vous parler en anglais (*locale* par défaut) ce qui permet d'être indépendant de la configuration linguistique du système
* Si vous passez l'argument `-y` à `apt` ou `dnf` il ne demande plus confirmation avant d'agir (mode non-interactif)
* Le plus simple pour déterminer si une commande executée à distance a réussi est de tester `stdout.channel.recv_exit_status() == 0`

**Allons-y !**

Vous pouvez choisir une distribution de GNU/Linux quelconque, préférentiellement CentOS 8 ou Debian 10.

La création initiale d'une machine virtuelle avec Virtual Box demande une intervention manuelle :
1.  Création de la machine avec réseau configuré en pont
2.  Import de la clef publique SSH de votre compte utilisateur sur la station Ubuntu (id_rsa.pub) dans le fichier /root/.ssh/authorized_keys
3.  Vérification que la configuration de sshd (/etc/ssh/sshd_config) permet à root de se connecter uniquement avec authentification à clef publique

Le déploiement des logiciels implique :
1.  Installation de Apache 2 et du module Apache php sur la première machine
2.  Installation de MariaDB sur la seconde machine
3.  Création d'une base et d'un compte utilisateur sur MariaDB pour la connexion de Wordpress
4.  Déploiement de Wordpress et configuration de Wordpress

Ces étapes seront rendues automatiques par l'appel à des scripts Python 3 utilisant le module client SSH Paramiko.

Dans un second temps on demande la mise en place sur la station Ubuntu d'une surveillance des deux serveurs :
1.  Exécution de commandes à distance avec Paramiko pour obtenir :
 *  La charge moyenne sur 1, 5 et 15mn, le nom de la machine, la date
 *  Optionnel : le nombre de processus httpd/apache2 en exécution
 *  Optionnel : l'espace disque disponible en pourcentage
2. Ces données seront insérées dans une base relationnelle (sqlite, MariaDB ou PostgreSQL) sur la station Ubuntu dans une base et une table que vous crérez avec une structure adéquate

La surveillance sera déclenchée par cron toutes les cinq minutes.

Le livrable comprend la description de l'architecture, les scripts, modules et fichiers de configuration ainsi qu'un document de
mise en œuvre à destination d'un•e technicien•e qui aurait à remonter la plate forme à partir de zéro.

